<?php

namespace Drupal\aaa_member_number\Parser;

class aaaMemberNumber implements aaaMemberNumberInterface
{

    private $club_number;
    private $household_length;
    private $firstmember;
    private $membernumber;

    function __construct( $club_number, $household_length, $firstmember) {
        $this->club_number = $club_number;
        $this->household_length = $household_length;
        $this->firstmember = $firstmember;
    }

    public function setMemberNumber($membernumber) {

        $this->membernumber = (string) $membernumber;

        if (!$this->isSixteen()) {
            throw new \Exception('The member number must be 16 digits.');
        }

        if (!$this->isCorrectClub()) {
            throw new \Exception('Club number must be ' . $this->club_number);
        }

        if(!$this->isNumericallyValid()) {
            throw new \Exception('This member number appears to be invalid.');
        }

        return TRUE;
    }

    protected function isSixteen() {
        if (strlen($this->getMemberNumber()) == 16) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    protected function isCorrectClub(){
        if ($this->getClubNumber() == $this->club_number) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    protected function isNumericallyValid() {
        if ($this->getCheckDigit() == $this->calcCheckdigit($this->getIso() . $this->getClubNumber() . $this->getHousehold() . $this->getAssociate())){
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    public function getIso() {
        return substr($this->getMemberNumber(), 0, 3);
    }

    public function getHousehold() {
        return substr($this->getMemberNumber(), 6, $this->household_length);
    }

    public function getAssociate() {
        return substr($this->getMemberNumber(), (6 + $this->household_length), (9 - $this->household_length));
    }

    public function getClubNumber() {
        return substr($this->getMemberNumber(), 3, 3);
    }

    public function getCheckDigit() {
        return substr($this->getMemberNumber(), 15, 1);
    }

    public function getMemberNumber() {
        return $this->membernumber;
    }

    public function getAllDetailsArray() {
        $return_array = array (
            'member_number' => $this->getMemberNumber(),
            'iso' => $this->getIso(),
            'club' => $this->getClubNumber(),
            'household' => $this->getHousehold(),
            'associate' => $this->getAssociate(),
            'check_digit' => $this->getCheckDigit(),
        );
        return $return_array;
    }

    public function calcCheckdigit($number) {
        $iterations = 1;
        while ($iterations-- >= 1) {
            $stack = 0;
            $number = str_split(strrev($number), 1);
            foreach ($number as $key => $value) {
                if ($key % 2 == 0) {
                    $value = array_sum(str_split($value * 2, 1));
                }
                $stack += $value;
            }
            $stack %= 10;
            if ($stack != 0) {
                $stack -= 10;
            }
            $number = implode('', array_reverse($number)) . abs($stack);
        }
        return abs($stack);
    }
}