<?php
/**
 * Created by PhpStorm.
 * User: garymorse
 * Date: 2/17/17
 * Time: 12:23 AM
 */
namespace Drupal\aaa_member_number\Parser;

interface aaaMemberNumberInterface
{
    public function setMemberNumber($membernumber);

    public function getIso();

    public function getHousehold();

    public function getAssociate();

    public function getClubNumber();

    public function getMemberNumber();

    public function getAllDetailsArray();
}