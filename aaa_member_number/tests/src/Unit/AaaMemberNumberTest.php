<?php

namespace Drupal\Tests\aaa_member_number\Unit;

use Drupal\aaa_member_number\Parser\aaaMemberNumber;
use Drupal\Tests\UnitTestCase;

/**
 * Tests generation of AAA Member Number.
 * @package Drupal\Tests\aaa_member_number\Unit
 * @group aaa_member_number
 */
class AaaMemberNumberTest extends UnitTestCase {

    public function testShortException() {
        // not 16 digits
        $member_number = '42900508766656';
        $aaamembernumber =  new aaaMemberNumber('005', 8, 1);
        $this->setExpectedException(\Exception::class);
        $aaamembernumber->setMemberNumber($member_number);
    }

    public function testClubException() {
        // wrong club
        $member_number = '4290024197185003';
        $aaamembernumber =  new aaaMemberNumber('005', 8, 1);
        $this->setExpectedException(\Exception::class);
        $aaamembernumber->setMemberNumber($member_number);
    }

    public function testNumericallyInvalidException() {
        // invalid check digit
        $member_number = '4290050876665618';
        $aaamembernumber =  new aaaMemberNumber('005', 8, 1);
        $this->setExpectedException(\Exception::class);
        $aaamembernumber->setMemberNumber($member_number);
    }

    public function testValidMemberData() {
        // Valid member number
        $member_number = '4290050876665619';
        $aaamembernumber =  new aaaMemberNumber('005', 8, 1);
        $aaamembernumber->setMemberNumber($member_number);

        $this->assertEquals($member_number, $aaamembernumber->getMemberNumber());
        $this->assertEquals('429', $aaamembernumber->getIso());
        $this->assertEquals('005', $aaamembernumber->getClubNumber());
        $this->assertEquals('08766656', $aaamembernumber->getHousehold());
        $this->assertEquals('1', $aaamembernumber->getAssociate());
        $this->assertEquals('9', $aaamembernumber->getCheckDigit());

        $return_array = array (
            'member_number' => '4290050876665619',
            'iso' => '429',
            'club' => '005',
            'household' => '08766656',
            'associate' => '1',
            'check_digit' => '9',
        );

        $this->assertEquals($return_array, $aaamembernumber->getAllDetailsArray());

    }
    
}