<?php

namespace Drupal\mdm\Controller;


use Drupal\Core\Controller\ControllerBase;
use Drupal\user\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;


class mdmController extends ControllerBase {

    private $store;

    /**
     * mdmForm constructor.
     */
    public function __construct(PrivateTempStoreFactory $tempstore)
    {
        $this->tempstore = $tempstore;
        $this->store = $this->tempstore->get('mdmform.member_number_data');
    }

    /**
     * {@inheritdoc}
     */
    public function getForm() {
        $build = array(
            '#type' => 'container',
        );
        $form_class = '\Drupal\mdm\Form\mdmForm';
        $build['form'] = \Drupal::formBuilder()->getForm($form_class);
        return $build;
    }

    public function getTitle() {
        if (!empty($this->store->get('member_number'))){
            $this->store->delete('member_number');
            unset($_SESSION['session_started']);
            return t("Membership Details");
        }
        else {
            return t("Enter your member number");
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container)
    {
        $tempstore = $container->get('user.private_tempstore');
        return new static($tempstore);
    }

}