<?php

namespace Drupal\mdm\Form;


use Drupal\aaa_member_number\Parser\aaaMemberNumber;
use Drupal\boomi\Service\DrupalBoomiServiceWrapper;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\SessionManagerInterface;
use Drupal\user\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;


class mdmForm extends FormBase {

    private $aaamembernumber;
    private $tempstore;
    private $sessionmanager;
    private $account;


    /**
     * mdmForm constructor.
     * @param aaaMemberNumber $aaamembernumber
     * @param PrivateTempStoreFactory $tempstore
     * @param SessionManagerInterface $sessionManager
     * @param AccountInterface $account
     * @param DrupalBoomiServiceWrapper $boomi
     */
    public function __construct(aaaMemberNumber $aaamembernumber, PrivateTempStoreFactory $tempstore, SessionManagerInterface $sessionManager, AccountInterface $account, DrupalBoomiServiceWrapper $boomi)
    {
        $this->aaamembernumber = $aaamembernumber;
        $this->tempstore = $tempstore;
        $this->sessionmanager = $sessionManager;
        $this->account = $account;
        $this->boomi = $boomi;

        if ($this->account->isAnonymous() && !isset($_SESSION['session_started'])) {
            $_SESSION['session_started'] = true;
            $this->sessionmanager->start();
        }

        $this->store = $this->tempstore->get('mdmform.member_number_data');
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'mdm_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {

        if (!empty($this->store->get('member_number'))) {
            $this->aaamembernumber->setMemberNumber($this->store->get('member_number'));
            $details = $this->boomi->getMemberInfo($this->aaamembernumber->getMemberNumber());

            $form['details'] = array(
                '#type' => 'container',
                'sometext' => array(
                    '#type' => 'markup',
                    '#markup' => '<pre>' . print_r($this->aaamembernumber->getAllDetailsArray(), TRUE) . '</pre>',
                ),
                'another' => array(
                    '#type' => 'markup',
                    '#markup' => '<pre>' . print_r($details->Membership, TRUE) . '</pre>',
                ),
                'reset' => array(
                    '#type' => 'markup',
                    '#markup' => '<a href="/enter-member-number">Try another</a>',
                ),
            );
        }
        else {

            $form['member_number'] = array(
                '#type' => "textfield",
                '#title' => "Member Number",
            );

            $form['actions']['#type'] = 'actions';
            $form['actions']['submit'] = array(
                '#type' => 'submit',
                '#value' => $this->t('Lookup'),
                '#button_type' => 'primary',
            );
        }
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        try {
            $this->aaamembernumber->setMemberNumber($form_state->getValue('member_number'));
        }
        catch (\Exception $e) {
            $form_state->setErrorByName('member_number',  $e->getMessage());
        }
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $this->aaamembernumber->setMemberNumber($form_state->getValue('member_number'));

        $this->store->set('member_number', $form_state->getValue('member_number'));
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container)
    {
        $aaamembernumber = $container->get('aaa.member_number');
        $tempstore = $container->get('user.private_tempstore');
        $session_manager = $container->get('session_manager');
        $current_user = $container->get('current_user');
        $boomi = $container->get('boomi_client');

        return new static($aaamembernumber, $tempstore, $session_manager, $current_user, $boomi);
    }

}