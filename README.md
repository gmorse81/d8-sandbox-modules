#Sandbox modules

These modules are designed to be installed in the mwg_membership D8 site, but are just for learning. 
The module mdm is dependent on aaa_membership_number and the boomi module. 

The aaa_membership_number module can be a standalone D8 service. 

All are not very well commented. Don't judge me. 

After installing (along with dependencies), you can go to `/enter-member-number` in your browser. 


##unit testing
to run unit tests: 

1. get a shell in the php container `bin/docker-compose exec php /bin/sh`
1. go to the core directory `cd core`
1. run the test against the group created for this class `../vendor/bin/phpunit --group=aaa_member_number`

